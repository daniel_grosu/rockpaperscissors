package com.rockpaperscissors;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Information {

	public static String[] moves = new String[] { "rock", "paper", "scissors" };

	public Information() {
		super();
	}

	public int logic(String move1, String move2) {
		if (!Arrays.stream(moves).anyMatch(move1::equals) || !Arrays.stream(moves).anyMatch(move2::equals))
			return -5; // never reached because there is no input from Users

		if (move1.equalsIgnoreCase(move2))
			return 0;

		if (move1.equalsIgnoreCase("rock") && move2.equalsIgnoreCase("paper")
				|| move1.equalsIgnoreCase("paper") && move2.equalsIgnoreCase("scissors")
				|| move1.equalsIgnoreCase("scissors") && move2.equalsIgnoreCase("rock"))
			return 2;
		else
			return 1;
	}

	public String random() {
		Random random = new Random();
		int result = random.nextInt(3);
		return moves[result];
	}

	public String whoWon(int result) {
		if (result == 0)
			return "Draw";
		if (result == 1)
			return "Player1 won";
		if (result == 2)
			return "Player2 won";
		return "You should not be here";
	}

	public Map<String, String> playRound() {
		String move1 = random();
		String move2 = "rock";
		String whoWon = whoWon(logic(move1, move2));
		Map<String, String> result = new HashMap<String, String>();
		result.put("move1", move1);
		result.put("move2", move2);
		result.put("result", whoWon);
		return result;
	}
}
