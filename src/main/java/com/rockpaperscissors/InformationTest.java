package com.rockpaperscissors;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class InformationTest {

	Information info = new Information();

	@Test
	void testLogic() {

		assertEquals(info.logic("rock", "scissors"), 1);
		assertEquals(info.logic("rock", "rock"), 0);
		assertEquals(info.logic("rock", "paper"), 2);

		assertEquals(info.logic("scissors", "scissors"), 0);
		assertEquals(info.logic("scissors", "rock"), 2);
		assertEquals(info.logic("scissors", "paper"), 1);

		assertEquals(info.logic("paper", "scissors"), 2);
		assertEquals(info.logic("paper", "rock"), 1);
		assertEquals(info.logic("paper", "paper"), 0);

		assertEquals(info.logic("paper", "incorrect"), -5);
		assertEquals(info.logic("incorrect", "rock"), -5);
	}

	@Test
	void testWhoWon() {
		assertEquals(info.whoWon(0), "Draw");
		assertEquals(info.whoWon(1), "Player1 won");
		assertEquals(info.whoWon(2), "Player2 won");
		assertEquals(info.whoWon(-5), "You should not be here");
	}

}
