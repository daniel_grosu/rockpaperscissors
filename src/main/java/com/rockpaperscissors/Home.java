package com.rockpaperscissors;

import java.io.IOException;
import java.util.Map;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(urlPatterns = { "/home" })
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Statistics totals = new Statistics();
	public int noRounds = 0;
	public int totalWins1 = 0;
	public int totalWins2 = 0;
	public int totalDraws = 0;

	/**
	 * Default constructor.
	 */
	public Home() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Information info = new Information();
		Map<String, String> result = info.playRound();

		String html = "<tr><td>" + result.get("move1") + "</td><td>" + result.get("move2") + "</td><td>"
				+ result.get("result") + "</td>";
		response.setContentType("text/plain");

		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(html);

		HttpSession session = request.getSession();
		session.setAttribute("noRounds", session.getAttribute("noRounds") == null ? 1 : noRounds++);

		if (result.get("result").equals("Draw"))
			session.setAttribute("totalDraws", session.getAttribute("totalDraws") == null ? 1 : totalDraws++);

		if (result.get("result").equals("Player1 won"))
			session.setAttribute("totalWins1", session.getAttribute("totalWins1") == null ? 1 : totalWins1++);

		if (result.get("result").equals("Player2 won"))
			session.setAttribute("totalWins2", session.getAttribute("totalWins2") == null ? 1 : totalWins2++);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("button2") != null) {
			noRounds = 0;
			request.setAttribute("noRounds", noRounds);
			RequestDispatcher dispatcher = request.getRequestDispatcher("firstPage.jsp");
			dispatcher.forward(request, response);
		}
	}

}
