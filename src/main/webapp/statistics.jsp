<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>

<script src='http://code.jquery.com/jquery-latest.min.js'></script>
<script>
$(document).on("click", "#button3", function() {
	location.reload(true);
});
</script>
<head>
<meta charset="ISO-8859-1">
<title>Rock Paper Scissors</title>
</head>
<body>

	<input type="submit" id="button3" name="button3" value="Reload scores" />
	
	<table id="gameHistory" style="width: 50%">
		<tbody>
			<tr>
				<th>Total rounds played</th>
				<th>Total wins first player</th>
				<th>Total wins second player</th>
				<th>Total draws</th>
			</tr>
			<tr>
				<td>${totalRounds}</td>
				<td>${totalWins1}</td>
				<td>${totalWins2}</td>
				<td>${totalDraws}</td>
			</tr>
		</tbody>
	</table>
</body>
</html>