<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>

<script src='http://code.jquery.com/jquery-latest.min.js'></script>
<script>
	$(document).on(
			"click",
			"#button1",
			function() {
				$.get("${pageContext.request.contextPath}/home", function(
						responseText) {
					$("#gameHistory > tbody").append(responseText);
					$('#counter').html(function(i, val) {
						return val * 1 + 1
					});
				});
			});

	$(document).on("click", "#button2", function() {
		$("#gameHistory > tbody").empty();
		$('#counter').html(function() {
			return 0;
		});
	});
</script>

<head>
<meta charset="ISO-8859-1">
<title>Rock Paper Scissors</title>
</head>
<body>

	<input type="submit" id="button1" name="button1" value="Play round" />
	<br>
	<input type="submit" id="button2" name="button2" value="Restart game" />
	<h1>
		No. of rounds played: <span id="counter">0</span>
	</h1>
	<br>

	<table id="gameHistory" style="width: 50%">
		<thead>
			<tr>
				<th>1st Player move</th>
				<th>2nd Player move</th>
				<th>result</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</body>
</html>